package com.dani3lviana.demo.repositories;

import com.dani3lviana.demo.models.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}