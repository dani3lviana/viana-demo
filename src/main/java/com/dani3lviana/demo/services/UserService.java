package com.dani3lviana.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.dani3lviana.demo.models.User;
import com.dani3lviana.demo.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public List<User> getAll() {
		List<User> users = new ArrayList<User>();
		userRepository.findAll().forEach(user -> users.add(user));

		return users;
	}

	public User getById(Long id) {
		return userRepository.findById(id).get();
	}

	public User save(User user) {
		return userRepository.save(user);
	}

	public void delete(Long id) {
		userRepository.deleteById(id);
	}
}