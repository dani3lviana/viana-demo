package com.dani3lviana.demo.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import com.dani3lviana.demo.models.User;
import com.dani3lviana.demo.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired
	UserService userService;
  
	@GetMapping("/users")
    private List<User> getAllUsers() {
		return userService.getAll();
	}

	@GetMapping("/users/{id}")
	private ResponseEntity<User> getUser(@PathVariable("id") Long id) {
		return ResponseEntity.ok(userService.getById(id));
	}

	@PostMapping("/users")
	private ResponseEntity<User> create(@RequestBody User user) {
		User savedUser = userService.save(user);

		return ResponseEntity.status(HttpStatus.CREATED)
				.contentType(MediaType.APPLICATION_JSON)
				.body(savedUser);
	}

	@PutMapping("/users")
	private ResponseEntity<User> update(@RequestBody User user) {
		User savedUser = userService.save(user);

		return ResponseEntity.ok(savedUser);
	}

	@DeleteMapping("/users/{id}")
	private ResponseEntity<Long> delete(@PathVariable("id") Long id) {
		userService.delete(id);
		return ResponseEntity.ok(id);
	}

	@ExceptionHandler({ NoSuchElementException.class })
    public ResponseEntity<User> handleException() {
		return ResponseEntity.notFound().build();
    }
}