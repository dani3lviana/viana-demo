package com.dani3lviana.demo.controllers;

import com.dani3lviana.demo.models.User;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTests {
	@Autowired
	private TestRestTemplate testRestTemplate;

	@Test
	public void shouldReturn200WhenRequestionAValidId() {
		@SuppressWarnings("rawtypes")
		ResponseEntity<User> entity = this.testRestTemplate.getForEntity("/users/5", User.class);

		assertThat(entity.getStatusCode(), is(HttpStatus.OK));
	}

	@Test
	public void shouldReturn404WhenRequestionAnInvalidId() {
		@SuppressWarnings("rawtypes")
		ResponseEntity<User> entity = this.testRestTemplate.getForEntity("/users/11", User.class);

		assertThat(entity.getStatusCode(), is(HttpStatus.NOT_FOUND));
	}

	@Test
	public void shouldReturnAListOfUsers() {
		@SuppressWarnings("rawtypes")
		ResponseEntity<List> entity = this.testRestTemplate.getForEntity("/users", List.class);

		assertThat(entity.getBody().size(), is(10));
	}
}