FROM jboss/wildfly

COPY build/libs/ROOT.war /opt/jboss/wildfly/standalone/deployments/

EXPOSE 8080